#define MAX_STEPS 100
#define MAX_DIST 100.0f
#define SURF_DIST 0.01f

//Define a mod as hlsl's works slightly differently
#define goodmod(x, y) ((x) - (y) * floor((x) / (y)))

cbuffer ScreenSizeBuffer : register(b1){
	float2 screenSize;
}

cbuffer TimeBuffer : register(b2){
	float time;
}

float2x2 rotate(float angle){
	float s = sin(angle);
	float c = cos(angle);
	return float2x2(c, -s, s, c);
}

//Ray march stuff
float getDist(float3 pos){
	float3 spherePos = float3(0, 1, 6);
	float sphereRad = 1.0f;
	
	float distToSphere = length(pos - spherePos) - sphereRad;
	float planeDist = pos.y; //Plane is at 0, 0, 0
	
	return min(distToSphere, planeDist);
}

float march(float3 origin, float3 direction){
	float totalDistance = 0.0f;
	
	for(int i = 0; i < MAX_STEPS; ++i){
		float3 pos = origin + direction * totalDistance;
		float sceneDist = getDist(pos);
		totalDistance += sceneDist;
		
		if(totalDistance > MAX_DIST || sceneDist < SURF_DIST){
			break;
		}
	}
	
	return totalDistance;
}

//Hexagon stuff
struct HexCell{
	float2 uv;
	float2 id;
};

float hexDistToPos(float2 pos){
	pos = abs(pos);

	float c = dot(pos, normalize(float2(1, 1.73f)));
	c = max(c, pos.x);

	return c;
}

HexCell getHexCoord(float2 uv){
	float2 repeat = float2(1.0f, 1.73f);
	float2 halfrep = repeat * 0.5f;

	float2 grida = goodmod(uv, repeat) - halfrep;
	float2 gridb = goodmod(uv - halfrep, repeat) - halfrep;

	float2 griduv = length(grida) < length(gridb) ? grida : gridb;

	HexCell cell;
	cell.uv = griduv;
	cell.id = uv - griduv;
	return cell;
}

float3 getNormal(float3 p){
	float dist = getDist(p);
	float2 e = float2(0.01f, 0.0f);
	
	float3 normal = dist - float3(getDist(p - e.xyy), getDist(p - e.yxy), getDist(p - e.yyx));
	return normalize(normal);
}

float getLight(float3 pointToShade){
	float3 lightPosition = float3(0, 5, 6);
	lightPosition.xz += float2(sin(time), cos(time)) * 2.0f;
	
	float3 dirToLight = normalize(lightPosition - pointToShade);
	float3 pointNomral = getNormal(pointToShade);
	
	float diffLightStrength = clamp(dot(dirToLight, pointNomral), 0.0f, 1.0f);
	
	//march towrads the light, if it's smaller than the distance to the light then we are in shadow
	float distToLight = march(pointToShade + pointNomral * SURF_DIST * 2.0f, dirToLight); //pointNomral * SURF_DIST because we need to move away from out current surface (else everything will be in shadow)
	if(distToLight < length(lightPosition - pointToShade)){
		diffLightStrength *= 0.1f;
	}
	
	return diffLightStrength;
}

float4 main(float2 tex : TexCoord) : SV_Target{
	float2 pixelPos = float2(tex.x * screenSize.x, tex.y * screenSize.y);
	float2 uv = (pixelPos - 0.5f * screenSize) / screenSize.y;

	float3 col = float3(0, 0, 0);

	//Adjust the uvs to be inside a hexagon
	uv *= 5.0f;
	uv.y += time;
	HexCell hc = getHexCoord(uv);
	uv = hc.uv;

	//Ray marching 2
	float3 rayOrigin = float3(0, 1, 0);
	float3 rayDir = normalize(float3(uv.x, uv.y, 1.0f));
	
	float rayDist = march(rayOrigin, rayDir);
	
	float3 currentPos = rayOrigin + rayDir * rayDist;
	
	float diffuseLight = getLight(currentPos);
	
	col += diffuseLight;


	return float4(col, 1.0f);
}