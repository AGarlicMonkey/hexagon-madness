#include <Clove/Clove.hpp>

using namespace clv;

int main(){
	Log::init();

	auto platform = plt::createPlatformInstance(plt::getPlatformPreferedAPI());
	auto& graphicsFactory = platform->getGraphicsFactory();

	auto mainWindow = platform->createWindow({"Hexagon Madness", 1280, 720});
	mainWindow->setVSync(true);

	std::string source_path = SOURCE_DIR;

	auto vertexShader = graphicsFactory.createShader({ gfx::ShaderStage::Vertex }, source_path + "/VertShader.hlsl");
	auto pixelShader = graphicsFactory.createShader({ gfx::ShaderStage::Pixel }, source_path + "/PixelShader.hlsl");

	auto pipeline = graphicsFactory.createPipelineObject();
	pipeline->setVertexShader(*vertexShader);
	pipeline->setPixelShader(*pixelShader);
	pipeline->setBlendState(true);
	pipeline->setCullMode(gfx::CullFace::Back, true);

	const std::vector<float> vertices = {
		-1.0f, -1.0f,  0.0f,  0.0f,
		 1.0f, -1.0f,  1.0f,  0.0f,
		-1.0f,  1.0f,  0.0f,  1.0f,
		 1.0f,  1.0f,  1.0f,  1.0f,
	};

	const std::vector<uint32_t> indices = {
		0, 1, 2,
		1, 3, 2
	};

	gfx::BufferDescriptor vbDesc{};
	vbDesc.elementSize	= sizeof(float) * 4.0f;
	vbDesc.bufferSize	= vertices.size() * sizeof(float);
	vbDesc.bufferType	= gfx::BufferType::VertexBuffer;
	vbDesc.bufferUsage	= gfx::BufferUsage::Default;
	auto vertexBuffer	= graphicsFactory.createBuffer(vbDesc, vertices.data());

	gfx::BufferDescriptor ibDesc{};
	ibDesc.elementSize	= sizeof(uint32_t);
	ibDesc.bufferSize	= indices.size() * sizeof(uint32_t);
	ibDesc.bufferType	= gfx::BufferType::IndexBuffer;
	ibDesc.bufferUsage	= gfx::BufferUsage::Default;
	auto indexBuffer	= graphicsFactory.createBuffer(ibDesc, indices.data());

	const mth::vec2f screenSize = mainWindow->getSize();

	gfx::BufferDescriptor ssDesc{};
	ssDesc.elementSize		= 0;
	ssDesc.bufferSize		= 16;
	ssDesc.bufferType		= gfx::BufferType::ShaderResourceBuffer;
	ssDesc.bufferUsage		= gfx::BufferUsage::Default;
	auto screenSizeBuffer	= graphicsFactory.createBuffer(ssDesc, &screenSize);

	float sec = 0.0f;

	gfx::BufferDescriptor tDesc{};
	tDesc.elementSize	= 0;
	tDesc.bufferSize	= 16;
	tDesc.bufferType	= gfx::BufferType::ShaderResourceBuffer;
	tDesc.bufferUsage	= gfx::BufferUsage::Dynamic;
	auto timeBuffer		= graphicsFactory.createBuffer(tDesc, &sec);

	auto commandBuffer = graphicsFactory.createCommandBuffer(mainWindow->getSurface());

	bool isWindowOpen = true;
	mainWindow->onWindowCloseDelegate.bind([&isWindowOpen](){ isWindowOpen = false; });

	auto prevFrameTime = std::chrono::system_clock::now();

	while(isWindowOpen){
		auto currFrameTime = std::chrono::system_clock::now();
		std::chrono::duration<float> deltaSeconds = currFrameTime - prevFrameTime;
		prevFrameTime = currFrameTime;

		sec += deltaSeconds.count();
		timeBuffer->updateData(&sec);

		mainWindow->beginFrame();

		commandBuffer->beginEncoding();

		commandBuffer->setViewport({ 0, 0, static_cast<int32_t>(screenSize.x), static_cast<int32_t>(screenSize.y) });
		commandBuffer->bindPipelineObject(*pipeline);
		commandBuffer->bindVertexBuffer(*vertexBuffer, static_cast<uint32_t>(pipeline->getVertexLayout().size()));
		commandBuffer->bindIndexBuffer(*indexBuffer);
		commandBuffer->bindShaderResourceBuffer(*screenSizeBuffer, gfx::ShaderStage::Pixel, 1u);
		commandBuffer->bindShaderResourceBuffer(*timeBuffer, gfx::ShaderStage::Pixel, 2u);

		commandBuffer->drawIndexed(indices.size());

		commandBuffer->endEncoding();

		mainWindow->endFrame();

		if(mainWindow->getKeyboard().isKeyPressed(Key::Escape)){
			break;
		}
	}

	return 0;
}